import React from 'react';
import {View,Text,StyleSheet,ScrollView} from 'react-native';
import { Icon } from 'react-native-elements';
import {colorScheme} from './variables';
import {getItem} from './Db';
import CalendarPage from './CalendarPage';
import ClassProfile from './ClassProfile';
import ClassList from './ClassList';
export default class ClassPage extends React.Component{
    state = {
        item: null,
        activeHeader: 'profile',
        id: this.props.screenProps.rootNav.state.params.id
    }
    componentDidMount(){
        // console.log(this.props);
        this.asyncGetItem();
    }
    render(){
        return(
            <View style={{borderWidth: 1,height: '100%',alignItems:'center',backgroundColor: '#99999920'}}>
                <View style={{width: '100%',backgroundColor: `${colorScheme}95`,height: '25%'}}>
                    {this.state.item != null ? 
                    <View style={{flex: 1, justifyContent: 'center'}}>
                        <Text style={{color: 'white', fontSize: 50,textAlign: 'center',fontWeight: 'bold'}}>{this.state.item.name}</Text>
                        {/* <Icon containerStyle={{paddingTop: 2}} name={"schedule"} size={20} color={colorScheme}/>  */}
                        <Text style={{color: 'white', fontSize: 25, textAlign: 'center'}}>S.Y {this.state.item.schoolyear}</Text>
                    </View>
                    : <Text></Text>}
                </View>
                <View style={styles.header}>
                    <View style={this.state.activeHeader == 'profile' ? styles.activeSubHeader : styles.subHeader}>
                        {/* <Text style={{textAlign: 'center',fontSize: 20,fontWeight: 'bold',color: colorScheme}}>28</Text> */}
                        <Text style={this.state.activeHeader == 'profile' ? styles.activeSubHeaderText : styles.subHeaderText} onPress={this.changeHeader.bind(this,'profile')}>Profile</Text>
                    </View>
                    <View style={this.state.activeHeader == 'attendance' ? styles.activeSubHeader : styles.subHeader}>
                        {/* <Text style={{textAlign: 'center',fontSize: 20,fontWeight: 'bold',color: colorScheme}}>Students</Text> */}
                        <Text style={this.state.activeHeader == 'attendance' ? styles.activeSubHeaderText : styles.subHeaderText} onPress={this.changeHeader.bind(this,'attendance')}>Attendance</Text>
                    </View>
                    <View style={this.state.activeHeader == 'students' ? styles.activeSubHeader : styles.subHeader}>
                        {/* <Text style={{textAlign: 'center',fontSize: 20,fontWeight: 'bold',color: colorScheme}}>Students</Text> */}
                        <Text style={this.state.activeHeader == 'students' ? styles.activeSubHeaderText : styles.subHeaderText} onPress={this.changeHeader.bind(this,'students')}>Students</Text>
                    </View>
                </View>
                {this.state.item == null ? <Text></Text> : 
                this.state.activeHeader == "profile" ?
                <ClassProfile item={this.state.item}/> : 
                this.state.activeHeader == "students" ?
                <ClassList renderType={"students"} navigateTo={'ClasClass'} sectionId={this.state.id} screenProps={{rootNav: this.props.screenProps.rootNav}}/> : <CalendarPage schoolYear={this.state.item.schoolyear} sectionId={this.state.id}/>
                }
            </View>
        );
    }
    async asyncGetItem(){
        let item = await getItem("sections",this.state.id);
        this.setState({
            item: item
        });
        console.log(this.state.item);
    }
    changeHeader = (i) => {
        this.setState({
            activeHeader: i
        })
        console.log(i);
    }
}

const styles = StyleSheet.create({
    header: {
        padding: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        flexDirection: 'row',
        justifyContent: 'space-around',
        elevation: 4,height: '7%',width: '95%',marginTop: -30,backgroundColor: 'white',
        borderRadius: 10
    },
    content: {
        padding: 5,
        alignSelf: 'center',marginBottom: 10,
        flexDirection: 'column',
        justifyContent: 'space-around',borderWidth: 0,
        width: '95%',backgroundColor: 'white'
    },
    subHeader: {
        justifyContent: 'center',height: '100%'
    },
    activeSubHeader: {
        justifyContent: 'center',height: '100%',borderBottomWidth: 2,borderBottomColor: colorScheme
    },
    subHeaderText: {
        textAlign: 'center',fontSize: 25
    },
    activeSubHeaderText: {
        textAlign: 'center',fontSize: 25,color: colorScheme
    }
})