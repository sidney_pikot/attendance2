import React from 'react';
import {SQLite} from 'expo';
import {Alert} from 'react-native';
const sections = `create table if not exists sections (id integer primary key not null, day text not null, hour text not null, 
    room text not null, adviser text not null, assistant text not null, name text not null,schoolyear text not null);`;
const students = `create table if not exists students (id integer primary key not null,name text not null,gender text not null, guardian text not null,
    contact text not null, address text not null,picture text,section_id integer not null,foreign key(section_id) references sections(id));`;
const attendance = `create table if not exists attendance (id integer primary key not null,a_date DATETIME not null, present integer not null default 0, 
                    student_id integer not null,section_id integer not null, foreign key(student_id) references students(id), foreign key(section_id) references sections(id));`;
// const student_section = `create table if not exists student_section (id integer primary key not null, student_id integer,section_id integer,
//                             foreign key(student_id) references students(id), foreign key(section_id) references sections(id));`;
const db = SQLite.openDatabase('attendance.db');
export const createTable = (table) => {
    db.transaction(tx=>{
        tx.executeSql(eval(table)),(t, error) => {
        console.log(error)},console.log(`create ${table} success`);
    })
}
export const dropTable = (table) => {
    db.transaction(tx=>{
        tx.executeSql(`drop table if exists ${table};`),(t,error) =>{
            console.log(error);
        },console.log(`delete ${table} successfull`);
    })
}
export const getItems = (table,page) => {
    return new Promise((resolve,reject) => 
    {
        db.transaction(tx => {
            tx.executeSql(
            `select * from ${table} limit 10 offset ${page};`,
            [],
            (_, { rows }) => {
                                resolve(rows._array);
                            },
                            (t, error) => {
                                reject({error: error});
                              }
            );
        })
    })
}
export const getItemsByClass = (table,id,page) => {
    return new Promise((resolve,reject) => 
    {
        db.transaction(tx => {
            tx.executeSql(
            `select * from ${table} where section_id = ? limit 10 offset ${page};`,
            [id],
            (_, { rows }) => {
                                resolve(rows._array);
                            },
                            (t, error) => {
                                reject({error: error});
                              }
            );
        })
    })
}
export const getItem = (table,id) => {
    return new Promise((resolve,reject) => 
    {
        db.transaction(tx => {
            tx.executeSql(
            `select * from ${table} where id = ?;`,
            [id],
            (_, { rows }) => {
                                resolve(rows._array[0]);
                            }
            );
        })
    })
}
export const getAllStudentAttendance = (id) => {
    return new Promise((resolve,reject) => 
    {
        db.transaction(tx => {
            tx.executeSql(
            `select a_date from attendance where student_id = ?;`,
            [id],
            (_, { rows }) => {
                                resolve(rows._array);
                            }
            );
        })
    })
}
export const dbGetDateWPA = (table,date,status) => {
    // console.log(date);
    return new Promise((resolve,reject) => 
    {
        db.transaction(tx => {
            tx.executeSql(
            // `select * from ${table} where strftime('%Y-%m',a_date) = ?;`,
            `select a_date from ${table} where present = ? and a_date between ? and ? group by a_date order by a_date asc ;`,
            [status,date[0],date[1]],
            (_, { rows }) => {
                                resolve(rows._array);
                                // console.log(rows._array);
                                // console.log("from db");
                            },
            (t,err) => {
                reject({error: err})
            }
            );
        })
    })
}
export const dbGetAttendanceByDate = (table,date,section_id) => {
    // console.log(date);
    return new Promise((resolve,reject) => 
    {
        db.transaction(tx => {
            tx.executeSql(
            // `select * from ${table} where strftime('%Y-%m',a_date) = ?;`,
            `select student_id,present from ${table} where a_date = ? and section_id = ? order by student_id asc ;`,
            [date,section_id],
            (_, { rows }) => {
                                resolve(rows._array);
                                // console.log(rows._array);
                                // console.log("from db");
                            },
            (t,err) => {
                reject({error: err})
            }
            );
        })
    })
}
export const dbAttendanceJoinStudent = (table,date,section_id) => {
    // console.log(date);
    return new Promise((resolve,reject) => 
    {
        db.transaction(tx => {
            tx.executeSql(
            // `select * from ${table} where strftime('%Y-%m',a_date) = ?;`,
            `select student_id,present,name from ${table} INNER JOIN students on students.id = attendance.student_id where attendance.a_date = ? and attendance.section_id = ? order by attendance.student_id asc ;`,
            [date,section_id],
            (_, { rows }) => {
                                resolve(rows._array);
                                // console.log(rows._array);
                                // console.log("from db");
                            },
            (t,err) => {
                reject({error: err})
            }
            );
        })
    })
}
export const dbGetStudentsByClass = (id) => {
    return new Promise((resolve,reject) => 
    {
        db.transaction(tx => {
            tx.executeSql(
            `select id,name
            from students 
            where section_id = ? order by id asc;`,
            [id],
            (_, { rows }) => {
                // console.log(rows._array);
                                resolve(rows._array);
                            },
            (t,err) => {
                reject({error: err})
            }
            );
        })
    })
}

export const addSectionItem = (obj) => {
    return new Promise((resolve,reject) => {db.transaction(
        tx => {
            tx.executeSql(`insert into sections (day,hour,room,adviser,assistant,name,schoolyear) values (?, ?, ?, ?, ?, ?,?)`,
                            [obj.day,obj.hour,obj.room,obj.adviser,obj.assistant,obj.name,obj.schoolyear],
                                (a,b)=>{tx.executeSql('select * from sections where id = ?;', [b.insertId], 
                                    (_, { rows }) =>
                                        {
                                            resolve(rows._array[0]);
                                        }
                                );
                            },
                                (t, error) => {
                                    reject({error: error});
                                }
                            );
        });
    });
}
export const addStudentItem = (obj) => {
    return new Promise((resolve,reject) => {db.transaction(
        tx => {
            tx.executeSql(`insert into students (name,gender,guardian,contact,address,picture,section_id) values (?, ?, ?, ?, ?, ?,?)`,
                            [obj.name,obj.gender,obj.guardian,obj.contact,obj.address,obj.picture,obj.sections],
                            (a,b)=>{tx.executeSql('select * from students where id = ?;', [b.insertId], 
                                (_, { rows }) =>
                                    {
                                        // console.log(rows._array[0].id)
                                        resolve(rows._array[0]);
                                    }
                             );
                            },
                            (t, error) => {
                                reject({error: error});
                              });
            
        });
    });
}
export const addAttendanceItem = (objs,day,section_id) => {
    console.log(objs);
    return new Promise((resolve,reject) => {db.transaction(
        tx => {
            tx.executeSql(`insert into attendance (a_date,student_id,section_id) values (?, ?, ?)`,
                            [day,objs.id,section_id],
                            (a,b)=>{tx.executeSql('select * from attendance where id = ?;', [b.insertId], 
                                (_, { rows }) =>
                                    {
                                        // console.log(rows._array[0])
                                        resolve(rows._array[0]);
                                    }
                             );
                            },
                            (t, error) => {
                                reject({error: error});
                              });
            
        });
    });
}
export const addAttendanceItems = (objs,a_date,section_id) => {
    return new Promise((resolve,reject) => {db.transaction(
        tx => {
            // for(var i = 0;i > objs.length;i++){
            tx.executeSql(`insert into attendance (a_date,student_id,section_id) values (?, ?, ?)`,
                            [a_date,objs.id,section_id],
                            (a,b)=>{tx.executeSql('select * from attendance where id = ?;', [b.insertId], 
                                (_, { rows }) =>
                                    {
                                        // console.log(rows._array[0])
                                        resolve(rows._array[0]);
                                    }
                             );
                            },
                            (t, error) => {
                                reject({error: error});
                              });
                            // }
            
        });
    });
}
export const deleteAttendance= (a_date,student_id) => {
    return new Promise((resolve,reject) => {
        db.transaction(
            tx => {
                tx.executeSql(`delete from attendance where a_date = ? and student_id = ?;`, [a_date,student_id], (a,b) =>{
                    // if (b.rowsAffected>0){
                    //     Alert.alert('Success','delete successful');
                    // }
                    resolve(b.rowsAffected);
                },(t,error) => {
                    reject({error: error})
                })
            }
        )
    })
}
export const deleteItem = (table,id) => {
    return new Promise((resolve,reject) => {
        db.transaction(
            tx => {
                tx.executeSql(`delete from ${table} where id = ?;`, [id], (a,b) =>{
                    // if (b.rowsAffected>0){
                    //     Alert.alert('Success','delete successful');
                    // }
                    resolve(b.rowsAffected);
                },(t,error) => {
                    reject({error: error})
                })
            }
        )
    })
}