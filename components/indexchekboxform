/**
 * react-native-checkbox-form
 * Checkbox component for react native, it works on iOS and Android
 * https://github.com/cuiyueshuai/react-native-checkbox-form.git
 */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  ScrollView,
  Text,
  Dimensions,
  TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

const WINDOW_WIDTH = Dimensions.get('window').width;

class CheckboxForm extends Component {
  constructor(props) {
    super(props);
    this.renderCheckItem = this.renderCheckItem.bind(this);
    this._onPress = this._onPress.bind(this);
    this.state = {
      dataSource: props.dataSource
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      dataSource: nextProps.dataSource
    });
  }

  static propTypes = {
    dataSource: PropTypes.array,
    formHorizontal: PropTypes.bool,
    labelHorizontal: PropTypes.bool,
    itemShowKey: PropTypes.string,
    itemCheckedKey: PropTypes.string,
    iconSize: PropTypes.number,
    iconColor: PropTypes.string,
    onChecked: PropTypes.func,
  };

  static defaultProps = {
    dataSource: [],
    formHorizontal: false,
    labelHorizontal: true,
    itemShowKey: 'label',
    itemCheckedKey: 'checked',
    iconSize: 15,
    iconColor: '#2f86d5',
  };

  _onPress(item, i) {
    const outputArr = this.state.dataSource.slice(0);
    outputArr[i] = item;
    this.setState({ dataSource: outputArr });
    if (this.props.onChecked) {
      this.props.onChecked(item);
    }
  }

  renderCheckItem(item, i) {
    const { itemShowKey, itemCheckedKey, iconSize, iconColor, textStyle } = this.props;
    const isChecked = item[itemCheckedKey] || false;

    return (
      <TouchableOpacity
        key={i}
        onPress={() => {
          item[itemCheckedKey] = !isChecked;
          this._onPress(item, i);
        }}
      >
        <View
          style={{height:40,padding:10,justifyContent: 'space-between',borderBottomWidth: 0.5,borderBottomColor: 'gray',flexDirection: this.props.labelHorizontal ? 'row' : 'column', alignItems: 'center' }}
        >
          <View
              style={{ marginRight: 5 }}
            >
            <Text style={{...textStyle}}>{'' + item[itemShowKey]}</Text>
          </View>
          <Icon
            name={isChecked ? "md-checkbox" : 'md-square-outline'}
            size={iconSize}
            color={isChecked ? "#68BC00" : 'gray'}
            raised
            underlayColor="red"
          />
          
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <ScrollView
        {...this.props}
        horizontal={ this.props.formHorizontal}
        style={[{ padding: 5,width: WINDOW_WIDTH}, this.props.style]}
      >
        {
          this.state.dataSource.map((item, i) => this.renderCheckItem(item, i))
        }
      </ScrollView>
    );
  }

}

export default CheckboxForm;
