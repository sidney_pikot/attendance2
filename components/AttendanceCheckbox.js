import CheckboxFormX from 'react-native-checkbox-form';
import React from 'react';
import {
    Text,
    View,
    StyleSheet
  } from 'react-native';
import {Alert} from 'react-native';
import { CheckBox} from 'react-native-elements';
import {addAttendanceItem,deleteAttendance,addAttendanceItems} from './Db';
export default class AttendanceCheckbox extends React.Component{
    state = {
        students: [],
        processing: false,
    }
    componentDidMount(){
        this.setState({students: this.props.students,shouldUpdate: this.props.shouldUpdate})
    }
    getAlert(){
        console.log(this.state.students);
        this.setState({processing: true});
        this.saveItems();
    }
    async saveItems(){
        for(let i = 0;i<this.state.students.length;i++){
            addAttendanceItems(this.state.students[i],this.props.day,this.props.section_id).then(value=> this.updateStudentAttendance(i))
            .catch(function(err){console.log(err)})
        }
    }
    updateStudentAttendance(index){
        this.state.students[index].selected = true
        index++;
        if(index == this.state.students.length){
            this.setState({students: this.state.students,processing: false});
            this.props.gotDates[this.props.day] ={};
            this.props.gotDates[this.props.day]= {selected: true,selectedColor: 'red'}
        }
        let isAllFalse = this.state.students.map(o=>o.selected);
        // if()
    }
    render(){
        return(
            <View>
                {this.state.processing == false ? <Text></Text> : <Text>Processing...</Text>}
                <CheckBox 
                    onPress={()=> this.checkTheBox()}
                    title="Click if all are present!"
                    checkedIcon='dot-circle-o'
                    uncheckedIcon='circle-o'
                    checked={this.state.checked}
                />
                <CheckboxFormX
                  style={{ width: "100%" }}
                  dataSource={this.state.students}
                  itemShowKey="name"
                  itemCheckedKey="selected"
                  iconSize={20}
                  textStyle={{fontSize: 20}}
                  formHorizontal={false}
                  labelHorizontal={true}
                  onChecked={(item) => this.makeAttendance(item,this.props.day,this.props.section_id)}
                />
            </View>
        );
    }
    checkTheBox(){
        return(
          Alert.alert(
            'Confirm','All will be marked present',
            [
              {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
              {text: 'Cancel',onPress: () => console.log('Cancel Pressed'),style: 'cancel',},
              {text: 'OK', onPress: () => {this.getAlert()}},
            ],
            {cancelable: false},
          ));
    }
    unCheckTheBox(){
        return(
          Alert.alert(
            'Confirm','All attendance for this day will be deleted.',
            [
              {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
              {text: 'Cancel',onPress: () => console.log('Cancel Pressed'),style: 'cancel',},
              {text: 'OK', onPress: () => {this.getAlert()}},
            ],
            {cancelable: false},
          ));
    }
    getAlert(){
        this.setState({processing: true});
        this.saveItems();
    }
    updateStudentAttendance(index){
        this.state.students[index].selected = true
        index++;
        if(index == this.state.students.length){
            this.setState({students: this.state.students,processing: false});
        }
        this.updateGotDates();
        this.setState({checked: true});
    }
    updateGotDates(){
        let isAllFalse = this.state.students.map(o=>o.selected);
        if(isAllFalse.includes(true)){
            this.props.gotDates[this.props.day] ={};
            this.props.gotDates[this.props.day]= {selected: true,selectedColor: 'red'}
        }else{
            this.props.gotDates[this.props.day] ={};
        }
    }
    async makeAttendance(student,day,section){
        if(student.selected == true){
            addAttendanceItem(student,day,section).then(value=>this.updateGotDates()).catch(function(err){console.log(err)})
        }else(
            deleteAttendance(day,student.id).then(value=>this.updateGotDates()).catch(function(err){console.log(err)})
        )
    }
    async saveItems(){
        for(let i = 0;i<this.state.students.length;i++){
            addAttendanceItems(this.state.students[i],this.props.day,this.props.section_id).then(value=> this.updateStudentAttendance(i))
            .catch(function(err){console.log(err)})
        }
    }
}