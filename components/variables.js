import React from 'react';
import {Dimensions} from 'react-native';
export const screenDimensions = Dimensions.get('window');
export const {height, width} = screenDimensions;
export const mainColor = "#33E9FF";
export const defaultFontSize = 20;
export const dangerButton = "red";
export const colorScheme = "#841584";
export const itemHeight = height / 6;
export const listContentHeight = itemHeight * 0.73;
export const studentsTable = "students";
export const sectionsTable = "sections";
export const currentYear = (new Date()).getFullYear();