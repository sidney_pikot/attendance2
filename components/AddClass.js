import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {View,KeyboardAvoidingView,TouchableOpacity,TouchableWithoutFeedback,ScrollView,TextInput,StyleSheet,Text,Keyboard, Button,Picker} from 'react-native';
import CustomTextInput from './CustomTextInput';
import {addSectionItem,addStudentItem,getItems} from './Db';
import {colorScheme,currentYear,defaultFontSize} from './variables';
export default class AddClass extends Component{
    state = {
        startTime: null,
        endTime: null,
        schoolyear: null,
        sectionss: [],
        section: null,
        newSection: {
            day: null,
            hour: null,
            room: null,
            adviser: null,
            assistant: null,
            name: null,
            schoolyear: null,
        },
        newStudent: {
            name: null,
            gender: null,
            guardian: null,
            contact: null,
            address: null,
            picture: null,
            sections: null
        }
    }
    componentDidMount(){
        console.log("hahaha");
        if (this.props.renderType == "students"){
            this.getSections('sections',0);
        }else{
            // this.setState({schoolyear: [`${currentYear}-${currentYear+1}`],startTime: "1AM",endTime: "1AM"});
        }
    }
    render(){
        let years = ["2018-2019","2019-2020"]
        let times = []
        let n = 1;
        while(n < 13){
            if (n==12){
                times.push(n+"PM");
            }
            else {
            times.push(n+"AM");
            }
            n++;
        }
        n=1;
        while(n < 13){
            if (n==12){
                times.push(n+"AM");
            }
            else {
            times.push(n+"PM");
            }
            n++;
        }
        return(
            <ScrollView
            onScroll={Keyboard.dismiss}
            style={styles.mainView}>
                
                    {this.props.renderType == 'sections' ?
                    <KeyboardAvoidingView>
                    <CustomTextInput ref="name" {...{title: "Section",placeHolder: "section's name"}} field="name" handText={this.fillNewSection.bind(this)} />
                    <CustomTextInput ref="adviser" {...{title: "Adviser",placeHolder: "adviser's name"}} field="adviser" handText={this.fillNewSection.bind(this)}/>
                    <CustomTextInput ref="assistant" {...{title: "Assistant",placeHolder: "assistants's name"}} field="assistant" handText={this.fillNewSection.bind(this)}/>
                    <CustomTextInput ref="room" {...{title: "Room",placeHolder: "room number"}} field="room" handText={this.fillNewSection.bind(this)}/>
                    <CustomTextInput ref="days" {...{title: "Days",placeHolder: "ex: MWF, TTH, M-F"}} field="day" handText={this.fillNewSection.bind(this)}/>
                    <View style={{marginBottom: 20}}>
                        <Text
                            style={{color: 'black', fontSize: defaultFontSize }}>
                            Time
                        </Text>
                        <View style={{flexDirection:"row",borderBottomWidth: 1.5,borderBottomColor: 'gray'}}>
                        <Picker
                            itemStyle={{color: 'gray',fontSize: 10,borderBottomWidth: 1.5}}
                            selectedValue={this.state.startTime}
                            style={{width: "50%"}}
                            onValueChange={(itemValue, itemIndex) => this.setState({startTime: itemValue})}
                        >
                            {this.renderPickerItems(times)}
                        </Picker>
                        <Picker
                            itemStyle={{color: 'gray',fontSize: 10,borderBottomWidth: 1.5}}
                            selectedValue={this.state.endTime}
                            style={{width: "50%"}}
                            onValueChange={(itemValue, itemIndex) => this.setState({endTime: itemValue})}
                        >
                            {this.renderPickerItems(times)}
                        </Picker>
                        </View>
                    </View>
                    {/* <CustomTextInput ref="time" {...{title: "Time",placeHolder: "ex: 10am-12nn, 1pm-3pm"}} field="hour" handText={this.fillNewSection.bind(this)}/> */}
                    <View style={{borderBottomWidth: 1.5,borderBottomColor: 'gray'}}>
                        <Text
                            style={{color: 'black', fontSize: defaultFontSize }}>
                            School year
                        </Text>
                        <Picker
                            itemStyle={{color: 'white',fontSize: 10}}
                            selectedValue={this.state.schoolyear}
                            onValueChange={(itemValue, itemIndex) => this.setState({schoolyear: itemValue})}
                        >
                            {this.renderPickerItems(years)}
                        </Picker>
                    </View>
                    </KeyboardAvoidingView>
                   : <KeyboardAvoidingView>
                    <CustomTextInput ref="name" {...{title: "Fullname",placeHolder: "student's name"}} field="name" handText={this.fillNewStudent.bind(this)} />
                    <CustomTextInput ref="gender" {...{title: "Gender",placeHolder: "gender"}} field="gender" handText={this.fillNewStudent.bind(this)}/>
                    <CustomTextInput ref="guardian" {...{title: "Guardian",placeHolder: "guardian's name"}} field="guardian" handText={this.fillNewStudent.bind(this)}/>
                    <CustomTextInput ref="contact" {...{title: "Contact",placeHolder: "guardian's contact number"}} field="contact" handText={this.fillNewStudent.bind(this)}/>
                    <CustomTextInput ref="address" {...{title: "Address",placeHolder: "address"}} field="address" handText={this.fillNewStudent.bind(this)}/>
                    <CustomTextInput ref="picture" {...{title: "Picture",placeHolder: "pic"}} field="picture" handText={this.fillNewStudent.bind(this)}/>
                    <View style={{borderBottomWidth: 1.5,borderBottomColor: 'gray'}}>
                        <Text
                            style={{color: 'black', fontSize: defaultFontSize }}>
                            Class
                        </Text>
                        {this.state.sectionss == null ? <Text></Text> :
                        <Picker
                            itemStyle={{color: 'white',fontSize: 10}}
                            selectedValue={this.state.section}
                            onValueChange={(itemValue, itemIndex) => {this.setState({section: itemValue});
                            this.setState(Object.assign(this.state.newStudent,{sections:itemValue}));
                            console.log(itemValue)}}
                        >
                            <Picker.Item style={{color:'white',opacity: 0.5}} key={'r-'} label={""} value={null} />
                            {this.renderPickerSections(this.state.sectionss)}
                        </Picker>}
                    </View>
                    </KeyboardAvoidingView>
                }
                <View style={styles.viewButton}>
                    <TouchableOpacity
                        style={styles.addButton}
                        onPress={this.addItemss.bind(this)}>
                        <Text style={styles.buttonText}>
                            Add 
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.addButton}
                        onPress={this.props.renderType == 'sections' ?  this.clearSectionFields.bind(this) : this.clearStudentFields.bind(this)}>
                        <Text style={styles.buttonText}>
                            Clear Fields
                        </Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
    async getSections(item,page){
        getItems(item,page).then(value => this.setState({sectionss: value})).catch(function(err){
            console.log(err);
        });
    }
    renderPickerItems = (data) => {
        let items = data.map((value, index) => {
            return (<Picker.Item style={{color:'white',opacity: 0.5}} key={'r-' + index} label={'' + value} value={value} />)
        })
        return items;
    }
    renderPickerSections = (data) => {
        let items = data.map((value, index) => {
            return (<Picker.Item style={{color:'white',opacity: 0.5}} key={'r-' + index} label={'' + value.name} value={value.id} />)
        })
        return items;
    }
    fillNewSection = (field,text) => {
            this.setState(Object.assign(this.state.newSection,{[field]:`${text}`}));
    };
    fillNewStudent = (field,text) => {
            this.setState(Object.assign(this.state.newStudent,{[field]:`${text}`}));
    }
    clearSectionFields = () => {
        this.refs.name.clearFieldss();
        this.refs.adviser.clearFieldss();
        this.refs.assistant.clearFieldss();
        this.refs.room.clearFieldss();
        this.refs.days.clearFieldss();
    }
    clearStudentFields = () => {
        this.refs.name.clearFieldss();
        this.refs.gender.clearFieldss();
        this.refs.guardian.clearFieldss();
        this.refs.contact.clearFieldss();
        this.refs.picture.clearFieldss();
        this.refs.address.clearFieldss();
    }
    addItemss = () => {
        this.props.renderType == 'sections' ? this.addSectionItem() : this.addStudentItem()         
    }
    addedItem(v) {
        this.props.screenProps.rootNav.state.params.returnData(v);
        this.props.screenProps.rootNav.goBack();
    }
    async addSectionItem() {
        this.setState(Object.assign(this.state.newSection,{hour:`${this.state.startTime}-${this.state.endTime}`}));
        this.setState(Object.assign(this.state.newSection,{schoolyear:`${this.state.schoolyear}`}));

        console.log(this.state.newSection);
        addSectionItem(this.state.newSection).then(value => this.addedItem(value)).catch(function(err){
            console.log(err);
        });
    };
    async addStudentItem() {
        addStudentItem(this.state.newStudent).then(value => this.addedItem(value)).catch(function(err){
            console.log(err);
        });
    };
}

const styles = StyleSheet.create({
    mainView: {
        paddingTop: 20,
        paddingLeft: 30,
        paddingRight:30,
    },
    viewButton: {
        flex: 1, 
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems:'center',
    marginTop: 30
    }
    ,addButton: {
        width: "45%",
        height: 50,
        borderRadius: 25,
        // flex: 1,
        justifyContent: 'center',
        backgroundColor: colorScheme,
        // alignSelf: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 2,
        marginBottom: 5,
    },
    buttonText: {
        textAlign: 'center',
        color: 'white',
        fontSize: 20
    }
});