import React, { Component } from 'react';
import {dbGetDateWPA,dbGetStudentsByClass,addAttendanceItems,addAttendanceItem,dbAttendanceJoinStudent} from './Db';
import moment from "moment";
import {Text,View,StyleSheet,ActivityIndicator} from 'react-native';
import { CheckBox} from 'react-native-elements';
import {Agenda} from 'react-native-calendars';
import {colorScheme} from './variables';
import AttendanceCheckbox from './AttendanceCheckbox';
export default class CalendarPage extends Component {
  constructor(props) {
    super(props);
    this.is_mounted = false,
    this.state = {
      mounted: 1,
      gotDates:{},
      students: [],
      status: 0,
      r1:[],
      refreshing: false,
      schoolYear: [],
      items: {[moment(Date.now()).format('YYYY-MM-DD')]: [{"name": "Fuck you"}]}
    };
  }
  componentDidMount(){
    this.is_mounted = true;
    console.log(this.is_mounted);
    this.is_mounted && this.setState({schoolYear: [`${this.props.schoolYear.split('-')[0]}-05-31`,`${this.props.schoolYear.split('-')[1]}-07-31`]},function(){
    this.getItems();
    });
    this.is_mounted && this.getStudentsOfClass();
  }
  componentWillUnmount() {
   this._isMounted = false;
}
  render() {
    return (
      this.state.mounted == 1 ? 
      <View style={[styles.activityIndicator]}>
        <ActivityIndicator size="large" color={colorScheme} />
      </View> :
      <Agenda
        style={{width: "100%"}}
        items={this.state.items}
        onDayPress={(day) => this.setItem(day.dateString)}
        renderItem={this.renderItem.bind(this)}
        renderEmptyDate={day => this.renderEmptyDate(day)}
        rowHasChanged={this.rowHasChanged.bind(this)}
        minDate={this.state.schoolYear[0]}
        maxDate={this.state.schoolYear[1]}
        theme={{
          agendaKnobColor: colorScheme
        }}
        onRefresh={() => console.log('refreshing...')}
        refreshing={this.state.refreshing}
        refreshControl={null}
        markedDates={
          this.state.gotDates
        }
      />
    );
  }

  async setItem(day){
    this.state.students.map(o => o.selected = false)
    dbAttendanceJoinStudent("attendance",day,this.props.sectionId).then(value=> 
      {
       this.successgetAttendanceByDate(value,day);
      }
      ).catch(function(err){console.log(err)})
  }

  async getItems(){
    dbGetDateWPA("attendance",this.state.schoolYear,this.state.status).then(value=>
      this.loadItems(value)
      )
    .catch(function(err){
      console.log(err);
    })
  }

  async a(selecteds,currentD){
    addAttendanceItems(selecteds,currentD,0,this.props.sectionId).then(value => console.log(value)).catch(function(err){console.log(err)})
  }

  async getStudentsOfClass(){
    dbGetStudentsByClass(this.props.sectionId).then(value=>this.successGetStudents(value))
    .catch(function(err){
      console.log(err);
    })
  }

  successgetAttendanceByDate(value,day){
    if(value.length > 0){
      this.state.students.map(o=> {
        for(let i =0;i<value.length;i++){
          if(o.id == value[i].student_id){
            o.selected = true;
            value.splice(i,1);
            break;
          }
        }
      })
    }
    this.is_mounted && this.setState({items: {[day]: []}});
  }

  loadItems(val){
    for(let i = 0;i <val.length;i++){
      this.state.gotDates[val[i].a_date]={}
      this.state.gotDates[val[i].a_date] = {selected: true,selectedColor: 'red'}
    }
    this.is_mounted && this.setState({mounted: 0});
  }

  successGetStudents(students){
    this.is_mounted && this.setState({students: students}
      )
  }

  renderItem(item) {
    return (
      <View style={styles.item}><Text>{item.name}</Text></View>
    );
  }
  renderEmptyDate(day) {
    const currentD = this.timeToString(day);
    return (
      this.state.students.length == 0 ? <View><Text>No Students</Text></View> : 
      <View>
        <AttendanceCheckbox students={this.state.students} gotDates={this.state.gotDates} day={currentD} section_id={this.props.sectionId}/>
     </View>
    );
  }
  rowHasChanged(r1, r2) {
    return r1.length !== r2.length;
  }
  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  },
  activityIndicator: {
    flex: 1,
    justifyContent: 'center'
  }
});