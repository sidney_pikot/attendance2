import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {View,TextInput,StyleSheet,Text} from 'react-native';
import {mainColor,colorScheme,defaultFontSize} from './variables';
export default class CustomTextInput extends Component{
    state = {
        borderBottomColors: "grey",
        labelColor: 'black',
        returnField: "",
        clearFields: false,
    }
    render(){
        return(
            <View>
                <Text
                    style={{color: this.state.labelColor, fontSize: defaultFontSize }}>
                    {this.props.title}
                </Text>
                <TextInput
                    keyboardType="default"
                    autoCapitalize="characters"
                    onChangeText={text=> {this.setState({returnField: text});}}
                    style={{borderBottomWidth: 1.5,borderBottomColor: this.state.borderBottomColors, marginBottom: 20}}
                    placeholder={this.props.placeHolder}
                    onFocus={this.handleFocus.bind(this)}
                    onBlur={this.handleBlur.bind(this)}
                    value={this.state.returnField}
                />
            </View>
        );
    }
    handleFocus = () => {
        this.setState({
            borderBottomColors: colorScheme,
            labelColor: colorScheme
        })
    }
    handleBlur = () => {
        this.setState({
            borderBottomColors: 'grey',
            labelColor: 'black'
        })
        this.props.handText(this.props.field, this.state.returnField);
    }
    clearFieldss = () => {
        this.setState({
            returnField: ""
        });
        // console.log(this.state.returnField + " hahaha");
    }
}

const styles = StyleSheet.create({
    input: {
        borderBottomWidth: 0.5,
    }
});