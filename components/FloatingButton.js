import React from 'react';
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';

class Fab extends React.Component{
    render(){
        return(
            <ActionButton buttonColor="rgba(231,76,60,1)" ref="actionButton">
            <ActionButton.Item buttonColor='#9b59b6' title={`Add new ${this.props.renderType}`} onPress={() => this.props.screenProps.rootNav.navigate('AddClass')}>
                <Icon name="md-create"/>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#3498db' title="Notifications" onPress={() => {}}>
                <Icon name="md-notifications-off"/>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#1abc9c' title="All Tasks" onPress={() => {}}>
                <Icon name="md-done-all"/>
            </ActionButton.Item>
            </ActionButton>
        );
    }
}
export default Fab;