import React, { Component } from "react";
import { View, Text, FlatList, ActivityIndicator,StyleSheet } from "react-native";
import { ListItem, SearchBar } from "react-native-elements";
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Fab from './FloatingButton';
import ListRow from './ListRow';
import { Constants, SQLite } from 'expo';
import {itemHeight,colorScheme} from './variables';
import {createTable,getItems,addItem,deleteItem,dropTable,getItemsByClass} from './Db';
let table =  []

class ClassList extends React.PureComponent {

  constructor(props) {
    super(props);
    this.onEndReachedCalledDuringMomentum = true;
    this.state = {
      table: null,
      loading: false,
      data: [],
      page: 0,
      seed: 1,
      loadMore: 0,
      error: null,
      refreshing: false,
      visibleButton: false,
      showSearch: false,
      searchKey: ""
    };
  }
  componentDidMount() {
    this.setState({table: this.props.renderType});
    this.renderAll();
  }
  render() {
    return (
        <View style={{flex: 1, width: '100%'}}>
        {this.state.data.length == 0 ? 
        <View style={[styles.activityIndicator]}>
          <ActivityIndicator size="large" color={colorScheme} />
        </View> : 
        <FlatList
        //   extraData={this.state.data}
          data={this.state.data}
          renderItem={({ item }) => (
            <ListRow {...item} navigateTo={this.props.navigateTo} renderType={this.props.renderType} onLongPress={() => this.removeItem(item)} screenProps={{rootNav: this.props.screenProps.rootNav}}/>
          )}
          keyExtractor={item => item.id.toString()}
        //   ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          maxToRenderPerBatch={5}
          initialNumToRender={5}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReachedThreshold={0.5}
          onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
          onEndReached={this.handleLoadMore}
          initialNumToRender={6}
        />}
        {this.state.visibleButton ?  
          <ActionButton buttonColor="rgba(231,76,60,1)" ref="actionButton">
            <ActionButton.Item buttonColor='#9b59b6' title={`Add new ${this.state.table}`} onPress={() => this.props.screenProps.rootNav.navigate('AddClass', {returnData: this.addAnItem.bind(this)})}>
                <Icon name="md-create"/>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#3498db' title="Search" onPress={() => {this.setState({showSearch: true})}}>
                <Icon name="md-notifications-off"/>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#1abc9c' title="All Tasks" onPress={() => dropTable(this.props.renderType)}>
                <Icon name="md-done-all"/>
            </ActionButton.Item>
            </ActionButton> : null}
        </View>
    );
  }
  // list
 renderAll = () => {
    this.setState({
        page: this.state.page + 10
    })
    this.makeRemoteRequest();
 }
// add
 addAnItem(l){
  this.setState({data: this.state.data.concat([l])});
}
// delete
async removeItem(item){
    
    deleteItem(this.props.renderType,item.id).then(value => this.successDelete(item)
    ).catch(function(err){
      alert("error deleting");
    })
};
// get list from db
  async makeRemoteRequest() {
    if (this.props.navigateTo == 'Profile'){
      getItems(this.props.renderType, this.state.page).then(value => this.successGet(value))
      .catch(function(err){
        console.log(err);
      })
    }else{
      getItemsByClass(this.props.renderType,this.props.sectionId,this.state.page).then(value=> this.successGet(value))
      .catch(function(err){
        console.log(err);
      })
    }
  };
  successDelete(item){
    console.log(item);
    table = this.state.data;
    let index = this.state.data.indexOf(item);
    table.splice(index,1);
            this.setState({
              data: [...table]
    });
  }
  successGet(v){
      this.setState({
        loadMore: v.length < 10 || v.empty ? 0 : 1
      })
    this.setState({
      data: this.state.data.concat(v),
      loading: false,
      refreshing: false,
      visibleButton: true,
      showSearch: false
    });
  }
  // refresh list
  handleRefresh = () => {
      table = [];
    this.setState(
      {
        page: 0,
        data: [],
        loadMore: 0,
        loading: false,
        seed: this.state.seed + 1,
        refreshing: true,
        visibleButton: false,
      },
      () => {
        this.renderAll();
      }
    );
  };

  // load more
  handleLoadMore = () => {
    if (this.state.loadMore == 1){
      this.setState({loading: true});
      if(!this.onEndReachedCalledDuringMomentum){
        this.renderAll();
          this.onEndReachedCalledDuringMomentum = true;
      }
  }
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };
  updateSearch = searchKey => {
    this.setState({ searchKey });
  };
  renderHeader = () => {
    if (!this.state.showSearch) return null;
    return (
    <View>
      <SearchBar placeholder="Type Here..." onChangeText={this.updateSearch} value={this.state.searchKey} lightTheme round />
    </View>
    )
  };

  renderFooter = () => {
    if (!this.state.loading) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
}

export default ClassList;


const styles = StyleSheet.create({
    item: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 10,
        margin: 5,
        padding: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,

        elevation: 4,
        flexDirection: 'row',
        // borderBottomColor: '#d6d7da',
        height: itemHeight
    },
    textStyle: {
        fontSize: 30,
    },
    activityIndicator: {
      flex: 1,
      justifyContent: 'center'
    }
});