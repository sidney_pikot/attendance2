import React from 'react';
import {View,Text,StyleSheet,ScrollView} from 'react-native';
import {colorScheme} from './variables';

const ClassProfile = (props) => {
    return(
            <ScrollView showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} style={{width: '100%',marginTop: 10}}>
                    <Text style={{fontSize: 20,color: '#666666',fontWeight: 'bold',paddingLeft: 3}}>Basic Info</Text>
                    <View style={styles.content}>
                        <View>
                            <Text style={{height: 15,color: colorScheme,fontSize: 15,fontWeight:'bold'}}>Adviser</Text>
                            <Text style={{height: 20,fontSize: 20,paddingLeft: 3,lineHeight: 20}}>{props.item.adviser}</Text>
                        </View>
                        <View>
                            <Text style={{height: 15,color: colorScheme,fontSize: 15,fontWeight:'bold'}}>Assistant</Text>
                            <Text style={{height: 20,fontSize: 20,paddingLeft: 3,lineHeight: 20}}>{props.item.assistant}</Text>
                        </View>
                        <View>
                            <Text style={{height: 15,color: colorScheme,fontSize: 15,fontWeight:'bold'}}>Schedule</Text>
                            <Text style={{height: 20,fontSize: 20,paddingLeft: 3,lineHeight: 20}}>{props.item.day} {props.item.hour}</Text>
                        </View>
                        <View>
                            <Text style={{height: 15,color: colorScheme,fontSize: 15,fontWeight:'bold'}}>Room</Text>
                            <Text style={{height: 20,fontSize: 20,paddingLeft: 3,lineHeight: 20}}>{props.item.room}</Text>
                        </View>
                    </View>
                </ScrollView>
    );
}
export default ClassProfile;

const styles = StyleSheet.create({
    content: {
        padding: 5,
        alignSelf: 'center',marginBottom: 10,
        flexDirection: 'column',
        justifyContent: 'space-around',borderWidth: 0,
        width: '95%',backgroundColor: 'white'
    }
})