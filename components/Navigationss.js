import React from 'react';
import {
  View,Text,StyleSheet,Button,StatusBar
} from 'react-native';
import {createSwitchNavigator,
  createAppContainer,
  createDrawerNavigator,
  createBottomTabNavigator,
  createStackNavigator,
  createMaterialTopTabNavigator} from 'react-navigation';
import Icon from '@expo/vector-icons/Ionicons';
//variables
import {colorScheme,studentsTable,sectionsTable} from './variables';
//screens
import ClassList from './ClassList';
import AddClass from './AddClass';
import ClassPage from './ClassPage';
import StudentList from './StudentList';
import StudentProfile from './StudentProfile';
import CalendarStudent from './CalendarStudent';
class Navigationss extends React.Component{
  componentDidMount(){
    StatusBar.setHidden(true);
  }
  render(){
    return(
      <AppContainer/>
    );
  }
}
export default Navigationss

class WelcomeScreen extends React.Component{
  render(){
    return(
      <View style={{flex: 1,alignItems: 'center', 'justifyContent': 'center'}}>
      <Button title="Login" onPress={()=> this.props.navigation.navigate('Dashboard')}/>
      <Button title="Sign" onPress={()=> console.log('sign')}/>

      <Text>Welcome</Text>
      </View>
    );
  }
}

//class
  class AddClassScreen extends React.Component{
    render(){
      return(
        <AddClass renderType={sectionsTable} screenProps={{rootNav: this.props.navigation}}/>
      )
    };
  }
  class ClassProfileScreen extends React.Component{
    render(){
      return(
        <ClassPage screenProps={{rootNav: this.props.navigation}}/>
      )
    };
  }
  class ClassListScreen extends React.Component{
    render(){
      return(
        <View style={{flex: 1,alignItems: 'center', 'justifyContent': 'center'}}>
        <ClassList navigateTo={'Profile'} renderType={sectionsTable} screenProps={{rootNav: this.props.navigation}}/>
        </View>
      );
    }
  }
  class ClasClass extends React.Component{
    render(){
      return(
        <CalendarStudent schoolYear={"2019-2020"} sectionId={2} screenProps={{rootNav: this.props.navigation}}/>
      );
    }
  }
  const ClassStack = createStackNavigator({
    ClassList: {screen: ClassListScreen,
    navigationOptions:({navigation}) =>{
      return{
        // headerTitle: 'Class',
        // headerRight:(
        //   <Icon
        //     style={{ paddingLeft: 10 }}
        //     onPress={() => navigation.openDrawer()}
        //     name="md-menu"
        //     size={30}
        //   />
        // )
        header: null
      }
    }},
    AddClass: {screen:  AddClassScreen},
    Profile: {
      screen: ClassProfileScreen,
      navigationOptions:({navigation}) => {
        return{
          header: null
        }
      }
    },
    ClasClass: {screen: ClasClass,
      navigationOptions:({navigation}) => {
        return{
          // header: null
        }
      }
    }
  })
//-----------
//student
  class StudentListScreen extends React.Component{
    render(){
      return(
        <View style={{flex: 1,alignItems: 'center', 'justifyContent': 'center'}}>
        <ClassList navigateTo={'Profile'} renderType={studentsTable} screenProps={{rootNav: this.props.navigation}}/>
        {/* <Text onPress={()=>this.props.navigation.navigate('Studentss')}>ahaha</Text> */}
        </View>
      );
    }
  }
  class Studentss extends React.Component{
    render(){
      return(
        <View style={{flex: 1,alignItems: 'center', 'justifyContent': 'center'}}>
        <StudentProfile/>
        <Text>ahaha</Text>
        </View>
      );
    }
  }
  class AddStudentScreen extends React.Component{
    render(){
      return(
        <AddClass renderType={studentsTable} screenProps={{rootNav: this.props.navigation}}/>
      )
    };
  }
  
const StudentStack = createStackNavigator({
  StudentList: {screen: StudentListScreen,
  navigationOptions:({navigation}) =>{
    return{
    header: null
    }
  }},
  AddClass: {screen: AddStudentScreen},
  Profile: {
    screen: Studentss,
    navigationOptions:({navigation}) => {
      return{
        header: null
      }
    }
  }
})
// ---------------------

const DashboardTabNavigator = createMaterialTopTabNavigator({
  Class: {screen: ClassStack,
    navigationOptions:({navigation}) => {
      const { routeName } = navigation.state.routes[navigation.state.index];
      if (routeName === "Profile" || routeName === "AddClass" || routeName==="ClasClass") {
        return{
          tabBarVisible: false,
          swipeEnabled: false
        }
      }
      
    }
  },
  Student: {screen: StudentStack,
    navigationOptions:({navigation}) => {
      const { routeName } = navigation.state.routes[navigation.state.index];
      if (routeName === "Profile" || routeName === 'AddClass') {
        return{
          tabBarVisible: false,
          swipeEnabled: false
        }
      }
      
    }}
},
{
  navigationOptions: ({ navigation }) => {
    const { routeName } = navigation.state.routes[navigation.state.index];
    return {
      headerTitle: routeName,
    
    };
  },
  tabBarOptions: {
    activeTintColor: colorScheme,
    inactiveTintColor: 'black',
    labelStyle: {
      fontSize: 15
    },
    indicatorStyle: {
      backgroundColor: colorScheme
    },
    style: {
      backgroundColor: 'white',
    },
  }
});
// const DashboardStackNavigator = createStackNavigator({
//   DashboardTabNavigator: DashboardTabNavigator
// },
// {
//   defaultNavigationOptions: ({ navigation }) => {
//     return {
//       headerLeft: (
//         <Icon
//           style={{ paddingLeft: 10 }}
//           onPress={() => navigation.openDrawer()}
//           name="md-menu"
//           size={30}
//         />
//       )
//     };
//   }
// });


const AppDrawerNavigator = createDrawerNavigator({
  Dashboard: {
    screen: DashboardTabNavigator
  }
});
const AppSwitchNavigator = createSwitchNavigator({
  // Welcome: {screen: WelcomeScreen},
  Dashboard: {screen: DashboardTabNavigator}
})
const AppContainer = createAppContainer(AppSwitchNavigator);