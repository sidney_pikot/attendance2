import React from 'react';
import moment from "moment";
import {Text,View,StyleSheet,ActivityIndicator} from 'react-native';
import {CalendarList} from 'react-native-calendars';
import {colorScheme} from './variables';
import {getAllStudentAttendance} from './Db';
export default class CalendarStudent extends React.Component {
    state = {
        id: this.props.screenProps.rootNav.state.params.id,
        isMounted: false,
        markedDates: {}
    }
    componentDidMount(){
        this.makeRemoteRequest();
    }
    render(){
        return(
            this.state.isMounted == false ? 
            <View style={[styles.activityIndicator]}>
                <ActivityIndicator size="large" color={colorScheme} />
            </View> :
            <CalendarList
            // Callback which gets executed when visible months change in scroll view. Default = undefined
            // onVisibleMonthsChange={(months) => {console.log('now these months are visible', months);}}
            // Max amount of months allowed to scroll to the past. Default = 50
            pastScrollRange={50}
            // Max amount of months allowed to scroll to the future. Default = 50
            futureScrollRange={50}
            // Enable or disable scrolling of calendar list
            scrollEnabled={true}
            // Enable or disable vertical scroll indicator. Default = false
            showScrollIndicator={true}
            markedDates={
          this.state.markedDates
        }
          />
        );
    }
    async makeRemoteRequest(){
        console.log(this.props.screenProps.rootNav.state.params.id);
        getAllStudentAttendance(this.state.id).then(value=> this.successGet(value))
        .catch(function(err){
            console.log(err);
        })
    }
    successGet(val){
        for(let i = 0;i <val.length;i++){
            this.state.markedDates[val[i].a_date]={}
            this.state.markedDates[val[i].a_date] = {selected: true,selectedColor: '#8bc34a'}
          }
        this.setState({isMounted: true});
    }
}

const styles = StyleSheet.create({
    activityIndicator: {
        flex: 1,
        justifyContent: 'center'
      }
})