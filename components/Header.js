import React, {Component} from 'react';
import {View} from 'react-native';
import { Header } from 'react-native-elements';
import {mainColor} from './variables';
class HeaderComponent extends Component{
    render() {
        return(
                <Header
                    placement="left"
                    leftComponent={{ icon: 'menu', color: "#fff"  }}
                    centerComponent={{ text: 'MY TITLE', style: { color: "#fff"  } }}
                    rightComponent={{ icon: 'home', color: "#fff" }}
                    containerStyle={{
                        backgroundColor: mainColor,
                    }}
            />
        );
    }
}

export default HeaderComponent;