
import React, { Component } from 'react';
import { ListItem, SearchBar,Avatar } from "react-native-elements";
import { View, Text, Image, StyleSheet, Animated, TouchableOpacity } from 'react-native';
import { Icon } from 'react-native-elements';
import TouchableScale from 'react-native-touchable-scale';
import {listContentHeight,itemHeight,colorScheme} from './variables';
const ANIMATION_DURATION = 250;
const ROW_HEIGHT = 70;

class TextIcon extends Component{
  render(){
    return(
      <View style={{height: '50%',width: '100%',flexDirection: 'row'}}>
          <Icon containerStyle={{paddingTop: 2}} name={this.props.iconData} size={20} color={colorScheme}/>
          <Text style={{paddingLeft:5,fontSize: 20}}>{this.props.textData}</Text>
      </View>
    );
  }
}
class SubtitleComponent extends Component{
  render(){
    const obj = this.props.obj;
    return(
      <View style={styles.subtitle}>
        <TextIcon iconData="person" textData="Plinky Nina Tamboboy"/>
        <TextIcon iconData="schedule" textData="MWF:10AM-12NN"/>
        <View style={{flexDirection: 'row',justifyContent: 'space-between',fontSize: 15,height: '33%'}}>
          <TextIcon iconData="school" textData="5"/>
          <TextIcon iconData="group" textData="28"/>
          <TextIcon iconData="present-to-all" textData="80%"/>
        </View>
      </View>
    );
  }
}
class ListRow extends Component {
  constructor(props) {
    super(props);

    this._animated = new Animated.Value(0);
  }

  componentDidMount() {
    Animated.timing(this._animated, {
      toValue: 1,
      duration: ANIMATION_DURATION,
    }).start();
  }

  onRemove = () => {
    const { onRemove } = this.props;
    if (onRemove) {
      Animated.timing(this._animated, {
        toValue: 0,
        duration: ANIMATION_DURATION,
      }).start(() => onRemove());
    }
  };

  render() {
    const { day,hour,room,adviser,assistant,name, id, gender,guardian,contact,address,picture} = this.props;
const avatars = this.props.renderType == 'sections' ? require('../assets/school.png') : gender == 'f' ? require('../assets/girl.png') : require('../assets/boy.png');

    const rowStyles = [
      styles.row,
      {
        height: this._animated.interpolate({
          inputRange: [0, 1],
          outputRange: [0, ROW_HEIGHT],
          extrapolate: 'clamp',
        }),
      },
      { opacity: this._animated },
      {
        transform: [
          { scale: this._animated },
          {
            rotate: this._animated.interpolate({
              inputRange: [0, 1],
              outputRange: ['35deg', '0deg'],
              extrapolate: 'clamp',
            })
          }
        ],
      },
    ];

    return (
      // <TouchableScale
      //     // onPress={this._onPressItem}
      //     // onLongPress={this._onLongPressItem}
      //     onLongPress={this.props.onLongPress}
      //     >
      <ListItem
        contentContainerStyle={styles.content}
        Component={TouchableScale}
        friction={90} //
        tension={100} // These props are passed to the parent component (here TouchableScale)
        activeScale={0.95}
        style={styles.item}
        roundAvatar
        key={id.toString}
        title={<View style={{width: '100%',height: '50%',justifyContent: 'flex-end'}}><Text style={{fontSize: 22,fontWeight: 'bold',color:colorScheme}}>{name}{id}</Text></View>}
        subtitle={this.props.renderType == 'sections' ? <TextIcon iconData="schedule" textData="MWF:10AM-12NN"/> : <TextIcon iconData="phone" textData={contact}/>}
        rightIcon={{name: 'keyboard-arrow-right'}}
        onLongPress={this.props.onLongPress}
        onPress={() => {this.props.screenProps.rootNav.navigate(this.props.navigateTo,{id: id});console.log(this.props.navigateTo)}}
        // avatar={{ uri: item.picture.thumbnail }}
        // leftAvatar={<Avatar rounded {{ source: ('../assets/boy.png') }} height={20}/>}
        
        leftAvatar={<Avatar title={name[0]} rounded source={avatars} size="large"/>}
        containerStyle={{ borderBottomWidth: 0 }}
        // badge={{ value: 3, textStyle: { color: 'orange' }, containerStyle: { marginTop: -20 } }}
      />
    // </TouchableOpacity>

    );
  }
}
const styles = StyleSheet.create({
    item: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 10,

        margin: 5,
        padding: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        }, shadowOpacity: 0.29,
        shadowRadius: 4.65,

        elevation: 4,
        flexDirection: 'row',
        // // borderBottomColor: '#d6d7da',
        height: itemHeight
    },
    content: {
      height: listContentHeight,
      alignItems: 'center',
      padding: 0
    },
    subtitle: {
      borderWidth: 1,
      flex: 1,
      flexDirection:'column',
      height:listContentHeight,width: '100%',fontSize: 12
    },
  row: {
    // flexDirection: 'row',
    // paddingHorizontal: 15,
    alignItems: 'center',
    // height: ROW_HEIGHT,
  },
  // image: {
  //   width: 50,
  //   height: 50,
  //   borderRadius: 25,
  //   marginRight: 10,
  // },
  // name: {
  //   fontSize: 18,
  //   fontWeight: '500',
  // },
  // email: {
  //   fontSize: 14,
  // },
});

export default ListRow;