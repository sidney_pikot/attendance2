import React, { Component } from "react";
import { View, Text, FlatList, ActivityIndicator,StyleSheet } from "react-native";
import { ListItem, SearchBar } from "react-native-elements";
import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';
import Fab from './FloatingButton';
import ListRow from './ListRow';
import { Constants, SQLite } from 'expo'
import {itemHeight} from './variables';
import {createTable,getItems,addItem,deleteItem} from './Db';
let sections =  []
class StudentList extends React.PureComponent {

  constructor(props) {
    super(props);
    this.onEndReachedCalledDuringMomentum = true;
    this.state = {
      loading: false,
      data: [],
      page: 0,
      seed: 1,
      loadMore: 0,
      error: null,
      refreshing: false,
      visibleButton: false,
      showSearch: false,
    };
  }
  componentDidMount() {
      console.log(this.props.renderType);
    createTable(this.props.renderType);
    this.renderAll();
    console.log(this.props.screenProps.rootNav);
  }
  render() {
    return (
        <View style={{flex: 1, width: '100%'}}>
        {this.state.data.length == 0 ? <Text>Empty List</Text> : <FlatList
        //   extraData={this.state.data}
          data={this.state.data}
          renderItem={({ item }) => (
            <ListRow {...item} onLongPress={this.removeItem.bind(this,item)} screenProps={{rootNav: this.props.screenProps.rootNav}}/>
          )}
          keyExtractor={item => item.id.toString()}
        //   ItemSeparatorComponent={this.renderSeparator}
          ListHeaderComponent={this.renderHeader}
          ListFooterComponent={this.renderFooter}
          maxToRenderPerBatch={5}
          initialNumToRender={5}
          onRefresh={this.handleRefresh}
          refreshing={this.state.refreshing}
          onEndReachedThreshold={0.5}
          onMomentumScrollBegin={() => { this.onEndReachedCalledDuringMomentum = false; }}
          onEndReached={this.handleLoadMore}
          initialNumToRender={6}
        />}
        {this.state.visibleButton ?  
          <ActionButton buttonColor="rgba(231,76,60,1)" ref="actionButton">
            <ActionButton.Item buttonColor='#9b59b6' title={`Add new sections`} onPress={() => this.props.screenProps.rootNav.navigate('AddClass', {returnData: this.addAnItem.bind(this)})}>
                <Icon name="md-create"/>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#3498db' title="Search" onPress={() => {this.setState({showSearch: true})}}>
                <Icon name="md-notifications-off"/>
            </ActionButton.Item>
            <ActionButton.Item buttonColor='#1abc9c' title="All Tasks" onPress={() => {}}>
                <Icon name="md-done-all"/>
            </ActionButton.Item>
            </ActionButton> : null}
        </View>
    );
  }
  // list
 renderAll = () => {
    this.setState({
        page: this.state.page + 10
    })
    this.makeRemoteRequest();
 }
// add
 addAnItem(l){
  this.setState({data: this.state.data.concat([l])});
}
// delete
removeItem = (item) => {
  sections = this.state.data;
    let index = this.state.data.indexOf(item);
    deleteItem(item.id);
  sections.splice(index,1);
  
  this.setState({
    data: [...sections]
  });
};
// get list from db
  async makeRemoteRequest() {
    const { page, seed } = this.state;
    results = await getItems(this.props.renderType,this.state.page);
    if (results.length == 10){
      this.setState({
        loadMore: 1
      })
    }else if (results.length < 10 || results.empty){
      this.setState({loadMore: 0})
    }
    this.setState({
        data: this.state.data.concat(results),
        loading: false,
        refreshing: false,
        visibleButton: true,
        showSearch: false
    });
  };
  // refresh list
  handleRefresh = () => {
      sections = [];
    this.setState(
      {
        page: 0,
        data: [],
        loadMore: 0,
        loading: false,
        seed: this.state.seed + 1,
        refreshing: true,
        visibleButton: false,
      },
      () => {
        this.renderAll();
      }
    );
  };

  // load more
  handleLoadMore = () => {
    if (this.state.loadMore == 1){
      this.setState({loading: true});
      if(!this.onEndReachedCalledDuringMomentum){
        this.renderAll();
          this.onEndReachedCalledDuringMomentum = true;
      }
  }
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "86%",
          backgroundColor: "#CED0CE",
          marginLeft: "14%"
        }}
      />
    );
  };

  renderHeader = () => {
    if (!this.state.showSearch) return null;
    return (
    <View>
      <SearchBar placeholder="Type Here..." lightTheme round />
    </View>
    )
  };

  renderFooter = () => {
    if (!this.state.loading) return null;
    return (
      <View
        style={{
          paddingVertical: 20,
          borderTopWidth: 1,
          borderColor: "#CED0CE"
        }}
      >
        <ActivityIndicator animating size="large" />
      </View>
    );
  };
}

export default StudentList;


const styles = StyleSheet.create({
    item: {
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 10,
        margin: 5,
        padding: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,

        elevation: 4,
        flexDirection: 'row',
        // borderBottomColor: '#d6d7da',
        height: itemHeight
    },
    textStyle: {
        fontSize: 30,
    }
});